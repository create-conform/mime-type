/////////////////////////////////////////////////////////////////////////////////////////////
//
// mime-type
//
//    Library that contains functions for mime-type detection.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var type  = require("type");

var ERROR_INVALID_BUFFER =    "Invalid Buffer";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////////////////
function MimeType(bytes) {
    //TODO - IMPLEMENT MORE TYPES, GOOD START BELOW
    //https://github.com/sindresorhus/file-type/blob/master/index.js
    //http://www.garykessler.net/library/file_sigs.html

    if (!type.isArray(bytes)) {
        throw new Error(Error.ERROR_INVALID_PARAMETER, "Expected an array, instead of '" + type.getType(bytes) + "'.");
    }
    if (bytes.length < 16) {
        throw new Error(ERROR_INVALID_BUFFER, "Expected an array containing at least 16 bytes, but got an array containing " + bytes.length + " bytes.");
    }

    var header = "";
    for (var i = 0; i < bytes.length; i++) {
        header += bytes[i].toString(16);
    }

    switch (header) {
        case "89504e47":
            return "image/png";
        case "47494638":
            return "image/gif";
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
            return "image/jpeg";
        default:
            // try file extension
            var name = own.getName();
            if (name) {
                var ext = name.substr(name.lastIndexOf("."));
                switch (ext) {
                    case ".svg":
                        return "image/svg+xml";
                    default:
                        return "unknown";
                }
            }
            break;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = MimeType;
module.exports.ERROR_INVALID_BUFFER =  ERROR_INVALID_BUFFER;